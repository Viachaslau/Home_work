package Somthing_new;
import com.sun.tools.javac.comp.Enter;

import java.util.Scanner;

/**
 * Created by User on 04.07.2017.
 */
public class Vova_wrote {
    private static final int ACTION_EXIT                = 0;
    private static final int ACTION_RUN_BUBBLE_SORT     = 1;
    private static final int ACTION_RUN_ARRAYS_SORT     = 2;
    private static final int ACTION_FILL_AN_ARRAY       = 3;

    private Scanner scanner;
    private int[] array;

    public static void main(String[] args) {
        Main app = new Main();
        app.runTheApp();
    }

    private void runTheApp() {
        fillAnArray();
    }

    private int readTheNumber() {
        return 0;
    }

    private void showMenu(){

    }

    private void runBubbleSort(){

    }

    private void runArraysSort(){

    }

    private void fillAnArray(){
        scanner = new Scanner(System.in);
        System.out.println("enter array elements delimit it by \",\" sign:");

        String tmp = "";
        if (!scanner.hasNextLine())
            return;
        tmp = scanner.nextLine();
        for (String string : tmp.split(",")){
            try {
                addArrayElement(Integer.parseInt(string));
            } catch (NumberFormatException e){
                System.out.printf("can't read the number from %s\n", string);
            }
        }
        System.out.println("Your ARRAY is: ");
        printAnArray();
    }

    private void printAnArray() {
        for (int i : array) {
            System.out.print(i + " ");
        }
    }

    private void addArrayElement(int element){
        if (array == null || array.length == 0){
            array = new int[1];
            array[0] = element;
            return;
        }
        int[] tpmArray = new int[array.length + 1];
        for(int i = 0; i < array.length; i ++){
            tpmArray[i] = array[i];
        }
        tpmArray[tpmArray.length - 1] = element;
        array = tpmArray;
    }

}

package Arrays;
import java.util.Random;
/**
 * Created by User on 29.06.2017.
 */
public class Snail {
    public static void main(String[] args) {
    int n = 8;
    int[][] snail = new int[n][n];
    int rows = 0;
    int columns = 0;
    int turn1 = 1;
    int turn2 = 0;
    int changes = 0;
    int visits = n;
    for (int i = 0; i < n * n; i++){
        snail[rows][columns]= i + 1;
        if (--visits == 0){
            visits = n * (changes % 2) +
                    n * ((changes + 1) % 2) -
                    (changes / 2 - 1) - 2;
            int temp = turn1;
            turn1 = -turn2;
            turn2 = temp;
            changes++;
        }
        columns += turn1;
        rows += turn2;
    }
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++)
            System.out.print(snail[i][j] + "\t");
        System.out.println();
    }
}
}

package Arrays;
import java.util.Random;
/**
 * Created by User on 04.07.2017.
 */
public class Bubble {
    public static void main(String[] args) {
        Bubble application = new Bubble();
        application.runTheApp();
    }
    int n = 50;
    private void runTheApp() {
        int[]arr1 = new int[n];
        int[]arr = new int[n];
        Random random = new Random();
        for (int i = 0; i < arr1.length; i++){
            arr1[i] = i + 1;
            System.out.print(arr1[i] + " ");
        }
        System.out.println();
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(n);
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println();
        for (int i = arr.length-1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (arr[j] > arr[j+1]){
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
        for (int i : arr) {
            System.out.print(i+" ");
        }
    }
}

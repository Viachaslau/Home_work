package Arrays;
import java.util.Random;
/**
 * Created by User on 29.06.2017.
 */
public class Matrix {
    public static void main(String[] args) {
    Matrix application = new Matrix();
    application.runTheApp();
}
    public void runTheApp(){
        int n = 8;
        int [][] matrix1 = new int[n][n];
        int [][] matrix2 = new int[n][n];
        Random random = new Random();
        System.out.println("\tMatrix 1\n");
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix1[i][j] = random.nextInt(n);
                System.out.print(matrix1[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("\tMatrix 2");
        System.out.println();
        for (int i =0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix2[i][j] = random.nextInt(n);
                System.out.print(matrix2[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("\t\tResult");
        System.out.println();
        int a = matrix1.length;
        int b = matrix2[0].length;
        int c = matrix2.length;
        int[][] result = new int[a][b];

        for (int i = 0;i < a; i++){
            for (int j = 0; j < b; j++){
                for (int k = 0; k < c; k++){
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        for (int i = 0 ; i < result.length; i++){
            for (int j = 0 ; j < result[0].length; j++){
                System.out.print(result[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
